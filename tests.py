from SIVLEOL_utils import *

def test_count_class():
    count_obj = count_class()
    
    count_obj.add_count("test")
    
    print count_obj["test"] == 0       
    
def test_remove_between_paranthesis_from_end_of_string():
    print [remove_between_paranthesis_from_end_of_string("test_string")] == ['test_string']
    print [remove_between_paranthesis_from_end_of_string("Dichloroethyl ether (Bis(2-chloroethyl)ether)")] == ['Dichloroethyl ether']
    print [remove_between_paranthesis_from_end_of_string("Bis(2-ethylhexyl)phthalate (DEHP)")] == ['Bis(2-ethylhexyl)phthalate']
    
def test_hmdb_id_to_7_digits():
    print convert_hmdb_id_to_7_digits("HMDB00374") == "HMDB0000374"
    
def test_capitalize_first_letter():
    print capitalize_first_letter("a Pan") == "A Pan"
    print capitalize_first_letter("Barrel roll") == "Barrel roll"
    print capitalize_first_letter("nom") == "Nom"
    print capitalize_first_letter("n") == "N"
    print capitalize_first_letter("") == ""
    
def run_tests():
    test_count_class()
    test_remove_between_paranthesis_from_end_of_string()
    test_hmdb_id_to_7_digits()
    test_capitalize_first_letter()
    
if __name__ == "__main__":
    run_tests()
    
#output should be:
#0
#['test_string']
#['Dichloroethyl ether']
#['Bis(2-ethylhexyl)phthalate']
#HMDB0000374    