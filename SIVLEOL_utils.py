# A place to keep my various utility things.

import xlrd, os
import unicodecsv as csv

class count_class:
    '''Class for keeping track of multiple counts, mostly just for easy printing.'''
    
    def __init__(self):
        self.key_list = [] #For preserving order.
        self.count_dict = dict() #Main structure
        
    def __getitem__(self, count_name):
        return self.count_dict[count_name]    
        
    def add_count(self, count_name):
        self.key_list.append(count_name)
        self.count_dict[count_name] = 0
        
    def add_counts(self, count_name_list):
        for count_name in count_name_list:
            self.add_count(count_name)
    
    def increment_count(self, count_name):
        self.count_dict[count_name] += 1
    
    def get_count(self, count_name):
        return self.count_dict[count_name]    
    
    def print_count(self, count_name):
        print count_name + ":", self.count_dict[count_name]
        
    def print_counts(self):
        '''Print all counts stored in given order.'''
        
        for key in self.key_list:
            self.print_count(key)

def remove_between_paranthesis_from_end_of_string(string):
    '''Remove paranthesis only at the end of a string.
    
    eg. "Dichloroethyl ether (Bis(2-chloroethyl)ether)" -> "Dichloroethyl ether"
    "Bis(2-ethylhexyl)phthalate (DEHP)" -> "Bis(2-ethylhexyl)phthalate"
    '''
    
    right_paranthesis_seen = 0
    left_paranthesis_seen = 0
    first_letter = 1
    
    new_string = ""
    
    for i in range(len(string)):
        if first_letter:
            first_letter = 0
            if string[-i-1] == ")":
                right_paranthesis_seen += 1
            else:
                return string #Only remove between first paranthesis if end of string is right paranthesis.
        else:
            if string[-i-1] == ")":
                right_paranthesis_seen += 1
            elif string[-i-1] == "(":
                left_paranthesis_seen += 1
                if left_paranthesis_seen == right_paranthesis_seen:
                    return string[:-i-1].strip()
                
    raise ValueError("Error: No left paranthesis for right paranthesis.")

def is_lower_case(string):
    
    if string != string.lower():
        return False
    else:
        return True
    
def reorganize_newlines_string_list(file_path, suffix="", seperator="; "):
    
    line_list = []
    
    with open(file_path) as f:
        for row in f:
            line = row.strip()
            line_list.append(line + suffix)
            
    print seperator.join(line_list)
            
def convert_hmdb_id_to_7_digits(hmdb_id):
    if hmdb_id.startswith("HMDB"):
        while len(hmdb_id) < 11:
            hmdb_id = hmdb_id[:4] + "0" + hmdb_id[4:]
        return hmdb_id
    else:
        print "Warning:", hmdb_id, "is not a HMDB ID, not converting."
        return hmdb_id
        
#From stack overflow. https://stackoverflow.com/questions/20105118/convert-xlsx-to-csv-correctly-using-python    
def csv_from_excel(input_path, output_path, sheet_name="Sheet1"):
    '''WARNING: Doesn't work properly for unicode.'''
    wb = xlrd.open_workbook(input_path)
    sh = wb.sheet_by_name(sheet_name)
    your_csv_file = open(output_path, 'w')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))

    your_csv_file.close()
    
def create_folder_if_not_exist(folder_path):
    
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
            
def capitalize_first_letter(string):
    if len(string) == 0:
        return string
    elif len(string) == 1:
        return string[0].upper()
    else:
        return string[0].upper()+string[1:]

def round_up_float(input_float, decimals):
    return math.ceil(input_float*10**decimals)/(10**decimals)
            
if __name__ == "__main__":
    print "For testing, see tests.py"
    
    reorganize_newlines_string_list("temp/temp_list.txt", suffix=" (NCI)")